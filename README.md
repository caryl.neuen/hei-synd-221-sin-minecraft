# SIn minecraft ElectricalAge controller
***
## Table of Contents
1. [General description](#general-description)
2. [Sin world](#sin-world)
3. [Producer](#producer)
4. [Consumers](#consumers)
5. [Control](#control)
6. [Program argument](#program-argument)
7. [How to run it](#how-to-run-it)
8. [Score](#score-max-on-a-single-regulation-without-random-condition)
9. [Utils files](#useful-files-and-links)
***
## General description
A programm designed to control the SIn world of mincraft ElectricalAge through a Modbus connection.     
>The control is performed with a SmartController class or through a web page in which we visualise the several inputs and outputs of the Minecraft World.
>All the data are sent to a database on InfluxDB and can be visualized with the Grafana website.

## Sin world
In this world :

##### Producer
* Solar Panel  : our main producer. On sunny days it gives enough power to produce energy and refill the battery.
* Wind power  : Is a weak source of production. Sometimes helps a bit to balance the consumers and producers.
* Coal factory  : the source of power that we fully control. It gives half the power of what the solar can do on sunny days. Be careful because it takes times to ramp up to the Setpoint we gives him (about 1 min).

##### Consumers
* Bunker : Where our controls panels are.
* Home : a domestic house that toggle on on given times, and sometimes randomly
* Factory : the source of our Score. we have to produce as many J as possible to get a high score.
* Public : the public lightning that toggle on at night.
***
## Control
The minecraft world is reguled by a smart control that toggle the different producers on and off to make sure the grid doesn't crash.
The battery state and the clock are the main variables of our regulation :
* We have to stay between 5% and 95% to prevent a battery issue (5% margin is enough)
* Clock is used to detect the night, where SolarPanel (our main producer), is useless and to detect the peak of domestic power at given times.

## Program argument
Grafana server              -->     https://influx.sdi.hevs.ch/write?db=SIn11 
Name of the Grafana groupe  -->     SIn11  
Host IP address             -->     Localhost   
Modbus port                 -->     1502  

## How to run it
To run the program, you have to download it and find the .jar file.

    <PathToMInecraftElectricalAge>/.../java_project/Minecraft.jar
    
Open it in command prompt and write :

    java -jar Minecraft.jar https://influx.sdi.hevs.ch/write?db=SIn11 SIn11 localhost 1502 -modbus4j


## Score max on a single regulation, without random condition
Score max = 484000 J

## Useful files and links
Index.html              --> In WebClient Folder     To open the web page  
grafana.sdi.hevs.ch     -->                         Link for Grafana

