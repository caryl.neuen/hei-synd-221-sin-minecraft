package ch.hevs.isi;


import ch.hevs.isi.core.Launcher;

import ch.hevs.isi.utils.Utility;

/**
*<p>Class used to lauch the program with the input of arguments in command line</p>
 * <p>List of several constant for our program:
 * <ul>
 *     <li> DBhostName : the influxDB server to connect to</li>
 *     <li>DBuserName : The name of the group in grafana.</li>
 *     <li>MBserver : the ip address of the cmoputer that will host the minecraft world</li>
 *     <li>MBport : the port of the cmoputer that will host the minecraft world</li>
 * </ul>
 *
 * @author Maxime Rouiller
 * @version 1.0
 * */
public class MinecraftController {
    public static boolean USE_MODBUS4J = false;
    public static String DBhostName;
    public static String DBuserName;
    public static String DBname;
    public static String DBpassword;
    public static String MBserver;
    public static int MBport;

    /**
     * print the arguments to use in case of an error in the command line
     */
    public static void usage() {
        System.out.println("Parameters: <InfluxDB Server> <Group Name> <MordbusTCP Server> <modbus TCP port> [-modbus4j]");
        System.exit(1);
    }

    /**
     * <p>our main class in the programm.</p>
     * <p>Sets the main variables of the program with the args[] array.</p>
     * <p>calls the launcher class that fill the datapoint list and take an instance of all component.</p>
     * @param args : takes the input arguments in command line that we can assign to our global variables.
     */
    public static void main(String[] args) {

        // Check the number of arguments and show usage message if the number does not match.
        String[] parameters = null;

        // If there is only one number given as parameter, construct the parameters according the group number.
        if (args.length == 4 || args.length == 5) {
            parameters = args;

            // Decode parameters for influxDB
            DBhostName  = parameters[0];
            DBuserName  = parameters[1];
            DBname      = DBuserName;
            DBpassword  = Utility.md5sum(DBuserName);

            // Decode parameters for Modbus TCP
            MBserver = parameters[2];
            MBport = Integer.parseInt(parameters[3]);

            if (args.length == 5) {
                USE_MODBUS4J = (parameters[4].compareToIgnoreCase("-modbus4j") == 0);
            }
        } else {
            usage();
        }

        Launcher.launch();






    }

}

