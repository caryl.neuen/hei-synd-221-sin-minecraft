package ch.hevs.isi.core;

import ch.hevs.isi.database.DatabaseConnector;
import ch.hevs.isi.field.FieldConnector;
import ch.hevs.isi.web.WebConnector;
/**
 *<p>Class used define the BinaryDatapoint.</p>
 * <p>We assign a boolean value to our datapoint.</p>

 * @author Maxime Rouiller, Caryl Neuenschwander
 * @version 1.0
 * */
public class BinaryDataPoint extends DataPoint {
    private boolean value;

    public BinaryDataPoint(String label, boolean isOutput, boolean value){
        super(label,isOutput);
        this.value=value;
    }

    /**
     * Set the new value of the Datapoint and call all the onNewValue methods to updtae our components.
     * @param value : the new value that we set.
     */
    public void setValue(boolean value){
        this.value=value;
        DatabaseConnector.getInstance().onNewValue(this);
        WebConnector.getInstance().onNewValue(this);
        if (isOutput()){
            FieldConnector.getInstance().onNewValue(this);
        }



    }

    /**
     * gives the value of the datapoint
     * @return
     */
    public  boolean getValue(){
        return value;
    }




}
