package ch.hevs.isi.core;




import javax.xml.crypto.Data;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 *<p>Class used define the Datapoint. It is the mother class of BinaryDataPoints and FloatDatapoint</p>
 * <p>When a Datapoint is created, it's put in a HashMap that keeps it in memory.</p>
 * <p>List of several constant for our program:
 * <ul>
 *     <li> label : the name of the datapoint</li>
 *     <li>isOutput : defines if we can write or not on a datapoint.</li>

 * </ul>
 *
 * @author Maxime Rouiller, Caryl Neuenschwander
 * @version 1.0
 * */
public class DataPoint {

    private String label;
    private boolean isOutput;

    private static HashMap<String, DataPoint> hm = new HashMap<>();

    public DataPoint(String label, boolean isOutput) {
        this.label = label;
        this.isOutput = isOutput;
        hm.put(label, this);
    }

    /**
     * <p>Used to get the Datapoint from is label</p>
     * @param label : the name of the datapoint
     * @return
     */
    public static DataPoint getDataPointFromLabel(String label) {
        return hm.get(label);

    }

    /**
     *
     * @return return the label of the datapoint
     */
    public String getLabel() {
        return label;

    }

    /**
     *
     * @return if the datapoint is an output or not.
     */
    public boolean isOutput() {
        return isOutput;
    }

    public static void main(String[] args) throws IOException {
        Launcher l = new Launcher();
        l.launch();



    }
}
