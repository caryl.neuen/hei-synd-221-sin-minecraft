package ch.hevs.isi.core;

public interface DataPointListener {
    void onNewValue(FloatDataPoint fdp);
    void onNewValue(BinaryDataPoint bdp);
}
