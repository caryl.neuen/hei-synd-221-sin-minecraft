package ch.hevs.isi.core;

import ch.hevs.isi.database.DatabaseConnector;
import ch.hevs.isi.field.FieldConnector;
import ch.hevs.isi.web.WebConnector;
import com.sun.corba.se.impl.orb.DataCollectorBase;
/**
 *<p>Class used define the FloatDatapoint.</p>
 * <p>We assign a float value to our datapoint.</p>

 * @author Maxime Rouiller, Caryl Neuenschwander
 * @version 1.0
 * */
public class FloatDataPoint extends DataPoint {
    private float value;

    public FloatDataPoint(String label, boolean isOutput, float value){
        super(label,isOutput);
        this.value=value;
    }
    /**
     * Set the new value of the Datapoint and call all the onNewValue methods to updtae our components.
     * @param value : the new value that we set.
     */
    public void setValue(float value){
        this.value=value;
        DatabaseConnector.getInstance().onNewValue(this);
        WebConnector.getInstance().onNewValue(this);
        if (isOutput()){
            FieldConnector.getInstance().onNewValue(this);
        }



    }
    /**
     * gives the value of the datapoint
     * @return
     */
    public  float getValue(){
        return value;
    }

}
