package ch.hevs.isi.core;

import ch.hevs.isi.database.DatabaseConnector;
import ch.hevs.isi.field.BooleanRegister;
import ch.hevs.isi.field.FieldConnector;
import ch.hevs.isi.field.FloatRegister;
import ch.hevs.isi.smart.SmartController;
import ch.hevs.isi.utils.Utility;
import ch.hevs.isi.web.WebConnector;

import java.io.*;

/**
 *<p>Class used to read the CSV files that contains all our datapoints.</p>
 * <p>It creates the datapoints with appropriate parameters </p>
 * <p>It creates the instance of all our components.</p>

 * @author Maxime Rouiller
 * @version 1.0
 * */

public class Launcher {
    /**
     * <p>this method reads the CSV file lines by lines.</p>
     * <p>It splits the line around the semicolon and puts all the strings in a array.</p>
     * <p>We used the values in this array to create our Boolean and Float Register.</p>
     * <p>It also creates the several Instances of our component</p>
     */
    public static void launch() {
        String line = "";
        String separator = ";";

        try {
            //parsing a CSV file into BufferedReader class constructor
            BufferedReader br = new BufferedReader(new FileReader("ModbusEA_SIn.csv"));
            while ((line = br.readLine()) != null)   //returns a Boolean value
            {

                String[] datapoint = line.split(separator);
                //System.out.println("Label=" + datapoint[0] + ", isOutput=" + datapoint[1] + ", value=" + datapoint[2]+ ", register="+datapoint[3]+", range="+datapoint[4]+", offset="+datapoint[5]);
                if (datapoint[2].equals("true") || datapoint[2].equals("false")){
                    BooleanRegister boolr = new BooleanRegister(datapoint[0], Boolean.parseBoolean(datapoint[1]), Boolean.parseBoolean(datapoint[2]), Integer.parseInt(datapoint[3]));
                }
                else {
                    FloatRegister floatr = new FloatRegister(datapoint[0], Boolean.parseBoolean(datapoint[1]), Float.parseFloat(datapoint[2]), Integer.parseInt(datapoint[3]),Integer.parseInt(datapoint[4]),Integer.parseInt(datapoint[5]) );                }

            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        DatabaseConnector.getInstance();
        WebConnector.getInstance();
        FieldConnector.getInstance();
        SmartController.getInstance();
    }



}

