package ch.hevs.isi.database;

import ch.hevs.isi.MinecraftController;
import ch.hevs.isi.core.BinaryDataPoint;
import ch.hevs.isi.core.DataPointListener;
import ch.hevs.isi.core.FloatDataPoint;
import ch.hevs.isi.utils.Utility;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Base64;

/**
*<p>Class for pushing datapoint to the Database</p>
 * <p>List of several constant for our HttpURL connection:
 * <ul>
 *     <li>URL : of course, the web site to connect to</li>
 *     <li>userpass : username and password to connect to Grafana Website </li>
 *     <li>encoding : the type of encoding requiered by Grafana to protect username and password</li>
 *     <li>type : the type of connection requiered by Grafana</li>
 * </ul>
 *
* @author Maxime Rouiller
* @version 1.0
* */

public class DatabaseConnector implements DataPointListener {
        // Private attribute
        private static DatabaseConnector db = null;
        //URL to connect to, doesn't change through the programm
        static String url= MinecraftController.DBhostName;
        // same as URL, doesn't change through programm
        static String userpass = MinecraftController.DBname+":"+MinecraftController.DBpassword;
        // encode the username and password to make them unreadable for clients
        String encoding = Base64.getEncoder().encodeToString(userpass.getBytes());
        // define the type of connection for HTML request
        String type = "binary/octet-stream";
        // Private constructor


        private DatabaseConnector(String url, String userpass) {
            this.url=url;
            this.userpass=userpass;
        }
        // The static method getInstance() returns a reference to the singleton.
        // It creates the single X_Connector object if it does not exist.
        /**
         * <p>The static method getInstance() returns a reference to the singleton.</p>
         * <p>It creates the single DataBase_Connector object if it does not exist.</p>
         * @return return the instance of Database Connector
         */
        public static DatabaseConnector getInstance() {
            if (db == null) { db = new DatabaseConnector(url,userpass); }
            return db;
        }


    /**
     *
     * <h2>
     * Send datapoints the the database, through HTML connection.
     * </h2>
     * <p>
     * The method use several object defined in the constructor of the DataBaseConnector class:
     * <ul>
     *     <li>The URL: stay the same through the process, and is used to establish connection with an HttpURLConnection object</li>
     *     <li>The encoding : encode the name and password of the user, to make them unreadable for clients.</li>
     *     <li>The type : define the type of cennection used in our Java-Grafana connection.</li>
     *
     * </ul>
     *
     * <p>An object OutputStreamWriter is used to send text messages on the Http connection
     * <em>(we use here writer.flush() to make sure the message is correctly send and not kept into computer memory)</em>
     * </p>
     * <p>In the end, a Response code is received, and if the message is "204"<em>(a no-error message)</em>, we simply receive the message, if not, we print the error code
     * </p>
     *
     * @throws IOException
     *            throws an IO exception
     *
     * @param label
     *            label of the Datapoint
     * @param value
     *            value of the Datapoint
     *
     */

    private void pushToDataBaseConnector(String label, float value) throws IOException {
        //create the object URL, with appropriate URL defined in constructor
        URL theURL=new URL(url);
        //open the connection to URL
        HttpURLConnection connection = (HttpURLConnection) theURL.openConnection();
        //these lines defines the properties of the connection
        connection.setRequestProperty("Authorization", "Basic "+encoding);
        connection.setRequestProperty("Content-Type", type);
        connection.setRequestMethod("POST");
        connection.setDoOutput(true);
        //create an object Outputstreamwriter to send some text through the URL connection
        OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
        //System.out.println(label+ " value="+value);
        //write the appropriate label and value on the output
        String s=label+ " value="+value;
        //make sure the f in the end of a float is deleted
        writer.write(s);
        //make sure the message is correctly send and not kept in computer memory
        writer.flush();
        //get the repsonse code from HTML connection
        int responseCode=connection.getResponseCode();
        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        //if the code is "204", which is a No-error connection, simply read the message received.
        if (responseCode==204){
            while( (in.readLine()) != null){

            }
        }
        else {
            System.out.println("error in the connection, code : "+responseCode);
        }
        //close the connection at the end of the operation
        connection.disconnect();

    }

    public static void main(String[] args) throws IOException {
        DatabaseConnector db= DatabaseConnector.getInstance();
        while(true) {
            db.pushToDataBaseConnector("Measurement", 11.054f);
            Utility.waitSomeTime(1000);
            db.pushToDataBaseConnector("Measurement", 4.9748f);
            Utility.waitSomeTime(1000);
            db.pushToDataBaseConnector("Measurement", 31.9389f);
            Utility.waitSomeTime(1000);
        }
    }

    @Override
    public void onNewValue(FloatDataPoint fdp) {
        try {
            float value=fdp.getValue();
            pushToDataBaseConnector(fdp.getLabel(),value);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onNewValue(BinaryDataPoint bdp) {
        try {
            if (bdp.getValue()==true){
                pushToDataBaseConnector(bdp.getLabel(),1);
            }
            else{
                pushToDataBaseConnector(bdp.getLabel(),0);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
