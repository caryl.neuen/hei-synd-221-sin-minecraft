package ch.hevs.isi.field;

import ch.hevs.isi.core.BinaryDataPoint;
import ch.hevs.isi.core.DataPoint;

import ch.hevs.isi.utils.Utility;
import java.util.HashMap;


import java.util.ArrayList;

/**
 * The instances of this class are the boolean modbus register
 */

public class BooleanRegister {

    int address;
    private static ArrayList<BooleanRegister> brList = new ArrayList<>();
    private static ModbusAccessor mbAccess = ModbusAccessor.getInstance();
    BinaryDataPoint dp;
    private static HashMap<DataPoint, BooleanRegister> BRhm = new HashMap<>();

    /**
     * The constructor of the class {@link BooleanRegister}
     * @param label
     * The label of the datapoint corresponding
     * @param isOutput
     * "1" if the {@link DataPoint} is  a output
     * @param value
     * Initial value of the {@link DataPoint}
     * @param address
     * Address modbus of the {@link DataPoint}
     */
    public BooleanRegister(String label, boolean isOutput, boolean value, int address) {
        this.dp = new BinaryDataPoint(label, isOutput, value);
        this.address = address;
        BRhm.put(dp, this);
        brList.add(this);
    }

    /**
     * To read the value of the register and set this value in the {@link DataPoint}
     */
    public void read (){
        try {
            Boolean value;
            value = mbAccess.readbool(address);
            dp.setValue(value);
        }catch (Exception e){
            e.printStackTrace();
            Utility.DEBUG("BooleanRegister","read","read is impossible");

        }

    }

    /**
     * To write the value of the {@link DataPoint} in the register
     */

        public void write(){
        try {
            boolean value;
            value = dp.getValue();
            mbAccess.writebool(address,value);
        }catch (Exception e){
            e.printStackTrace();
            Utility.DEBUG("BooleanRegister","write","write is impossible");
        }
    }

    /**
     * To update the value of all the register in each polling
     */
    public static void poll() {
        for (BooleanRegister br : brList) {
            br.read();
        }
    }

    /**
     * Find the {@link DataPoint} with the register
     * @return
     * Return the {@link DataPoint} of the register
     */
    public BinaryDataPoint getDataPoint() {
        return dp;
    }

    /**
     * Find the register with the {@link DataPoint}
     * @param bdp
     * The {@link DataPoint} corresponding of the register
     * @return
     * the register corresponding of the {@link DataPoint}
     */

    public static BooleanRegister getRegisterFromDataPoint(BinaryDataPoint bdp){
        return BRhm.get(bdp);
    }



}
