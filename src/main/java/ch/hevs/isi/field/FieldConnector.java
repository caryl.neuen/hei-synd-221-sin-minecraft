package ch.hevs.isi.field;

import ch.hevs.isi.MinecraftController;
import ch.hevs.isi.core.*;
import ch.hevs.isi.database.DatabaseConnector;
import ch.hevs.isi.utils.Utility;
import com.sun.xml.internal.bind.v2.runtime.reflect.opt.FieldAccessor_Double;

import java.util.Timer;
import java.util.TimerTask;

/**
 * class to link Modbus object (Float/DatapointRegister) with {@link DataPoint}
 */

public class FieldConnector implements DataPointListener {
    // Private attribute
    private static ModbusAccessor mbAccess = null;
    private static FieldConnector fc = null;
    private Timer polltimer;
    private String host = MinecraftController.MBserver;
    private int port=MinecraftController.MBport;

    ;

    /**
     * <p>
     * The constructor of the class {@link FieldConnector}.
     * </p>
     * <p>
     * It creates a Modbus connection with the class {@link ModbusAccessor}.
     * </p>
     * It creates the timer for the polling (polltimer)
     */
    private FieldConnector() {


        mbAccess = ModbusAccessor.getInstance();
        mbAccess.config(host,port);
        polltimer = new Timer();
        polltimer.scheduleAtFixedRate(new PollTask(),0, 3000);

    }
    // The static method getInstance() returns a reference to the singleton.
    // It creates the single X_Connector object if it does not exist.

    /**
     * Ensure that there is one and only one instance of {@link FieldConnector}
     * @return
     * return this instance
     */
    public static FieldConnector getInstance() {
        if (fc == null) {
            fc = new FieldConnector();
        }
        return fc;
    }
    private void pushToFieldConnector(String label, String value){


    }

    /**
     * Is executed when the value of the {@link DataPoint} changes
     * @param fdp
     * The {@link DataPoint} where the value changes
     */
    @Override
    public void onNewValue(FloatDataPoint fdp) {
        //System.out.println("the label in FC is "+ fdp.getLabel()+ " with value : "+ fdp.getValue());
        //System.out.println("");
        FloatRegister.getRegisterFromDataPoint(fdp).write();
    }
    /**
     * Is executed when the value of the {@link DataPoint} changes
     * @param bdp
     * The {@link DataPoint} where the value changes
     */

    @Override
    public void onNewValue(BinaryDataPoint bdp) {
        //System.out.println("the label in FC is "+ bdp.getLabel()+ " with value : "+ bdp.getValue());
        BooleanRegister.getRegisterFromDataPoint(bdp).write();

    }

    public static void main(String[] args) {


    }
}

