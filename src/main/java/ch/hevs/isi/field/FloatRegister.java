package ch.hevs.isi.field;


import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.FloatDataPoint;
import java.util.ArrayList;
import ch.hevs.isi.utils.Utility;

import java.util.HashMap;

/**
 * The instances of this class are the float modbus register
 */

public class FloatRegister {

    int address;
    int range;
    int offset;
    private static ModbusAccessor mbAccess = ModbusAccessor.getInstance();
    FloatDataPoint dp;
    private static HashMap<DataPoint, FloatRegister> FRhm = new HashMap<>();
    private static ArrayList<FloatRegister> frList = new ArrayList<>();

    /**
     * The constructor of the class {@link FloatRegister}
     * @param label
     * The label of the {@link DataPoint} corresponding
     * @param isOutput
     * "1" if the {@link DataPoint} is  a output
     * @param value
     * Initial value of the {@link DataPoint}
     * @param address
     * Address modbus of the {@link DataPoint}
     * @param range
     * Range of the register
     * @param offset
     * Offset of the register
     */

    public FloatRegister(String label,boolean isOutput, float value, int address,  int range, int offset){
        this.dp = new FloatDataPoint(label, isOutput, value);
        this.address = address;
        this.range=range;
        this.offset=offset;
        FRhm.put(dp, this);
        frList.add(this);
    }

    /**
     * To read the value of the register and set this value in the {@link DataPoint}
     */

    public void read (){
        try {
            Float value;
            value = range *(mbAccess.readfloat(address)) + offset;
            dp.setValue(value);
        }catch (Exception e){
            e.printStackTrace();
            Utility.DEBUG("FloatRegister","read","read is impossible");
        }

    }

    /**
     * To write the value of the {@link DataPoint} in the register
     */

    public void write(){
        try {
            Float value;
            value = dp.getValue();
            mbAccess.writefloat(address,(value-offset)/range);
        }catch (Exception e){
            e.printStackTrace();
            Utility.DEBUG("BooleanRegister","write","write is impossible");
        }
    }

    /**
     * To update the value of all the register each polling
     */

    public static void poll() {
        for (FloatRegister fr : frList) {
            fr.read();
        }
    }

    /**
     * Find the {@link DataPoint} with the register
     * @return
     * Return the {@link DataPoint} of the register
     */

    public FloatDataPoint getDataPoint() {
        return dp;
    }

    /**
     * Find the register with the {@link DataPoint}
     * @param bdp
     * The {@link DataPoint} corresponding of the register
     * @return
     * the register corresponding of the {@link DataPoint}
     */

    public static FloatRegister getRegisterFromDataPoint(FloatDataPoint bdp){
        return FRhm.get(bdp);
    }

}
