package ch.hevs.isi.field;
import ch.hevs.isi.core.Launcher;
import ch.hevs.isi.utils.Utility;
import com.serotonin.modbus4j.*;
import com.serotonin.modbus4j.code.DataType;
import com.serotonin.modbus4j.exception.ErrorResponseException;
import com.serotonin.modbus4j.exception.ModbusInitException;
import com.serotonin.modbus4j.exception.ModbusTransportException;
import com.serotonin.modbus4j.ip.IpParameters;
import com.serotonin.modbus4j.locator.BaseLocator;

import java.util.Timer;
import java.util.TimerTask;


/**
 * <h1>
 *     Class to create a modbus connection over TCP
 * </h1>
 */
public class ModbusAccessor {

    private static ModbusAccessor instance = null;
    private ModbusMaster modbus;
    private String host = "localhost";
    private int port = 1502;
    /**
     * <h1>
     * Constructor of the class {@link ModbusAccessor}
     * </h1>
     * <p>
     * This fonction :
     * <ul>
     *     <li>Initialize the modbus connection with IP parameters (host and port)</li>
     *     <li>Create a TCP master</li>
     * </ul>
     */
    private ModbusAccessor() {
        Timer polltimer = new Timer();
    //init connection
        IpParameters params = new IpParameters();
        params.setHost(host);
        params.setPort(port);
        modbus = new ModbusFactory().createTcpMaster(params, false);

        try {
            modbus.init();
        } catch (ModbusInitException e) {
            e.printStackTrace();
        }
    }

    /**
     * For the singleton {@link ModbusAccessor} with default IP parameters
     * <ul>
     *     <li>host default is "localhost"</li>
     *     <li>port default is 1502</li>
     * </ul>
     * @return
     * return the instance
     */


    public static ModbusAccessor getInstance() {
        if (instance == null) {
            instance = new ModbusAccessor();
        }
        return instance;
    }
    /**
     * For the singleton {@link ModbusAccessor} with chosen IP parameters
     * @param host
     * The host of the modbus connection
     * @param port
     * The port used for the modbus connection
     */
    public void config(String host, int port) {
        if (instance == null) {
            instance = new ModbusAccessor();
        }
        this.host = host;
        this.port = port;

    }

    /**
     *To read value of a {@link BooleanRegister}
     * @param register
     * register to read address
     *
     * @return
     * value of register or null if error
     */
    protected Boolean readbool (int register){
        try {
            return modbus.getValue(BaseLocator.coilStatus(1, register));
        } catch (ModbusTransportException e) {
            //e.printStackTrace();
        } catch (ErrorResponseException e) {
            //e.printStackTrace();
        }
        return null;
    }

    /**
     * To write a value in a {@link BooleanRegister}
     * @param register
     * register to write address
     * @param value
     * value to write in the {@link BooleanRegister}
     */
    protected void writebool (int register, boolean value){
        try {
            modbus.setValue(BaseLocator.coilStatus(1,register),value);
        } catch (ModbusTransportException e) {
            //e.printStackTrace();
        } catch (ErrorResponseException e) {
            //e.printStackTrace();
        }


    }

    /**
     *
     * To read a float register{@link FloatRegister}
     * {@link FloatRegister} address
     * @param register
     *  address of the register to read
     * @return
     * value of register or null if error
     */
    protected float readfloat (int register){

        try {
            return (float) modbus.getValue(BaseLocator.inputRegister(1,register, DataType.FOUR_BYTE_FLOAT));
        } catch (ModbusTransportException e) {
            //e.printStackTrace();
        } catch (ErrorResponseException e) {
            //e.printStackTrace();
        }
        return 0;
    }

    /**
     * To write in a {@link FloatRegister}
     * @param register
     *{@link FloatRegister} address
     * @param value
     * value to write in the register
     */
    protected void writefloat(int register, float value) {

        try {
            modbus.setValue(BaseLocator.holdingRegister(1,register, DataType.FOUR_BYTE_FLOAT),value);
        } catch (ModbusTransportException e) {
            //e.printStackTrace();
            Utility.DEBUG("ModbusAccessor", "main", "Write impossible");
        } catch (ErrorResponseException e) {
            //e.printStackTrace();
            Utility.DEBUG("ModbusAccessor", "main", "Write impossible");
        }
    }

    public static void main(String[] args) {
        ModbusAccessor mbAccess = ModbusAccessor.getInstance();
        boolean running = true;
        while (running) {

            mbAccess.writefloat(205,0.4f);
            float value = mbAccess.readfloat(89);

            if (value == -1f) {
                Utility.DEBUG("ModbusAccessor", "main", "Modbus connection error");
                running = false;
            } else {
                Utility.DEBUG("ModbusAccessor", "main", "Register 89: " + value);
                Utility.waitSomeTime(1000);
            }
        }
    }
    }
