package ch.hevs.isi.field;

import java.util.TimerTask;

/**
 * This class extends TimerTask and is instanced each period of the pollTimer in {@link FieldConnector}
 */

public class PollTask extends TimerTask {

    /**
     * This method is executed each period of the pollTimer and run the method {@link BooleanRegister#poll()} and {@link BooleanRegister#poll()}
     */

    public void run(){
        BooleanRegister.poll();
        FloatRegister.poll();
    }
}
