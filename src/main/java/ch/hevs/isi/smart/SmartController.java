package ch.hevs.isi.smart;

import ch.hevs.isi.core.BinaryDataPoint;
import ch.hevs.isi.core.FloatDataPoint;
import ch.hevs.isi.field.ModbusAccessor;

import java.util.Timer;
/**
 *<p>Class that runs the optimisation programm for our Minecraft World</p>
 * <p>List of serveral constant for our class:
 * <ul>
 *     <li>Timer : the object timer used to perform task each given second</li>
 *     <li>instance : used to perform our singleton model.  </li>
 * </ul>
 *
 * @author Maxime Rouiller
 * @version 1.0
 * */
public class SmartController {
    private static SmartController instance = null;




    private Timer SmartTimer ;
    FloatDataPoint score = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("SCORE");

    /**
     * <p>Constructor based on the singleton Model.</p>
     * <p>used to start the timer by serveral methods :</p>
     * <ul>
     *      <li>scheduleAtFixedRate : define the value in ms at which the methods are actualised</li>
     * </ul>
     *
     */
    private SmartController() {
        SmartTimer= new Timer();
        SmartTimer.scheduleAtFixedRate(new SmartTimer(),0,1000 );
        System.out.println(score.getValue());
    }

    /**
     * <p>The static method getInstance() returns a reference to the singleton.</p>
     * <p>It creates the single SmartController object if it does not exist.</p>
     * @return return the instance of SmartController
     */
    public static SmartController getInstance() {
        if (instance == null) {
            instance = new SmartController();
        }
        return instance;
    }


}
