package ch.hevs.isi.smart;

import ch.hevs.isi.core.BinaryDataPoint;
import ch.hevs.isi.core.FloatDataPoint;
import java.util.TimerTask;
/**
 *<p>Classthat defines the optimisation program of the Minecraft world</p>
 *
 * @author Maxime Rouiller
 * @version 1.0
 * */

public class SmartTimer extends TimerTask {

    /**
     * <p>This method manage the remoteSolar value, the remoteWind value, the
     * coalSetUp value and the factoryPower value that depend  of the BatteryLevel, and the clock Value.</p>
     * <p>The first if is there to compensate the high consumption of the house in the evening</p>
     */
    @Override
    public void run() {
        FloatDataPoint factoryPower = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("REMOTE_FACTORY_SP");
        BinaryDataPoint remoteSolar = (BinaryDataPoint) BinaryDataPoint.getDataPointFromLabel("REMOTE_SOLAR_SW");
        BinaryDataPoint remoteWind = (BinaryDataPoint) BinaryDataPoint.getDataPointFromLabel("REMOTE_WIND_SW");
        FloatDataPoint coalSetUp = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("REMOTE_COAL_SP");
        FloatDataPoint batteryLevel = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("BATT_CHRG_FLOAT");
        FloatDataPoint clock = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("CLOCK_FLOAT");



            //ce premier if est la pour amortir le pic de consommation de 19h qui défonce la batterie
            if (clock.getValue()>0.7f && clock.getValue()<0.833f && batteryLevel.getValue()<0.97f){
                remoteSolar.setValue(true);
                remoteWind.setValue(true);
                coalSetUp.setValue(0.8f);
                factoryPower.setValue(0.3f);
            }
            else if (batteryLevel.getValue() >= 0.97f) {
                remoteSolar.setValue(false);
                remoteWind.setValue(false);
                coalSetUp.setValue(0f);
                factoryPower.setValue(1f);

            }
            else if (batteryLevel.getValue() > 0.7f && batteryLevel.getValue() < 0.97f) {
                remoteSolar.setValue(true);
                remoteWind.setValue(true);
                coalSetUp.setValue(0.1f);
                factoryPower.setValue(1f);

            }
            else if (batteryLevel.getValue() > 0.5f && batteryLevel.getValue() <= 0.7f && clock.getValue()>0.25f && clock.getValue()<=0.6f) {
                remoteSolar.setValue(true);
                remoteWind.setValue(true);
                coalSetUp.setValue(0f);
                factoryPower.setValue(0.7f);//0.5f

            }
            else if (batteryLevel.getValue() > 0.5f && batteryLevel.getValue() <= 0.7f && (clock.getValue()<=0.25f || clock.getValue()>0.6f)) {
                remoteSolar.setValue(true);
                remoteWind.setValue(true);
                coalSetUp.setValue(0.4f);
                factoryPower.setValue(0.3f);//0.3f
                //System.out.println("test_night mode 50-70");
            }
            else if (batteryLevel.getValue() > 0.3f && batteryLevel.getValue() <= 0.5f && clock.getValue()>0.25f && clock.getValue()<=0.6f) {
                remoteSolar.setValue(true);
                remoteWind.setValue(true);
                coalSetUp.setValue(0.2f);
                factoryPower.setValue(0.3f);//0.3f

            }
            else if (batteryLevel.getValue() > 0.3f && batteryLevel.getValue() <= 0.5f && (clock.getValue()<=0.25f || clock.getValue()>0.6f)) {
                remoteSolar.setValue(true);
                remoteWind.setValue(true);
                coalSetUp.setValue(0.5f);
                factoryPower.setValue(0.4f);//0.2f
                //System.out.println("test_night mode 30-50");
            }


            else {
                remoteSolar.setValue(true);
                remoteWind.setValue(true);
                coalSetUp.setValue(0.8f);
                factoryPower.setValue(0f);
                //System.out.println("full_mode");
                //System.out.println(clock.getValue());

            }
        }
    }




