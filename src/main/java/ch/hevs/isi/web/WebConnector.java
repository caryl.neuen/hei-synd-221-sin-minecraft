package ch.hevs.isi.web;

import ch.hevs.isi.core.BinaryDataPoint;

import ch.hevs.isi.core.DataPointListener;
import ch.hevs.isi.core.FloatDataPoint;
import ch.hevs.isi.core.Launcher;
import ch.hevs.isi.utils.Utility;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

import java.net.InetSocketAddress;
import java.util.Vector;

/**
 *<p>Class for pushing datapoint to the web page</p>
 * <p>List of several constant for our class:
 * <ul>
 *     <li>Vector Websocket : list sockets active in the tcp connection</li>
 * </ul>
 *
 * @author Maxime Rouiller
 * @version 1.0
 * */

public class WebConnector extends WebSocketServer implements DataPointListener {

    // Private attribute

    private static WebConnector wc = null;
    Vector<WebSocket> v;

    /**
     * <p>Constructor based on the singleton Model.</p>
     * <p>Inherits method from WebSocketServer, such as InetSocketAdress() or start()</p>
     * <ul>
     *      <li>InetSocketAdress() is used to define the port of connection</li>
     *      <li>Start initialise the TCP connection between program and web page</li>
     * </ul>
     *
     */

    private WebConnector(){
        super(new InetSocketAddress(8888));
        this.start();
        v=new Vector<>();
    }


    // The static method getInstance() returns a reference to the singleton.
    // It creates the single X_Connector object if it does not exist.
    /**
     * <p>The static method getInstance() returns a reference to the singleton.</p>
     * <p>It creates the single Web_Connector object if it does not exist.</p>
     * @return return the instance of Web Connector
     */
    public static WebConnector getInstance(){
        if (wc == null) { wc = new WebConnector(); }
        return wc;
    }

    /**
     * <p>the method is called when a SetValue is done on a FloatDataPoint</p>
     * <p>Use the method Send() (from WebSocketServer) to sens a message to the web page with according protocol</p>
     * @param fdp : A new updated float datapoint
     */
    @Override
    public void onNewValue(FloatDataPoint fdp) {
        for (WebSocket ws : v){
            ws.send(fdp.getLabel()+ "="+String.valueOf(fdp.getValue()));

        }

        //System.out.println("the label in WC is "+ fdp.getLabel()+ " with value : "+ fdp.getValue());
    }
    /**
     * <p>the method is called when a {@link BinaryDataPoint#setValue(boolean)} is done on a BinaryDataPoint</p>
     * <p>Use the method Send() (from WebSocketServer) to sens a message to the web page with according protocol</p>
     * @param bdp : A new updated boolean datapoint
     */
    @Override
    public void onNewValue(BinaryDataPoint bdp) {
        for (WebSocket ws : v){
            ws.send(bdp.getLabel()+ "="+String.valueOf(bdp.getValue()));


        }
        //System.out.println("the label in WC is "+ bdp.getLabel()+ " with value : "+ bdp.getValue());
    }


    /**
            * <p>Method inherited from WebSocketServer</p>
     * <p>Establish the connection between program and web page</p>
     * @param webSocket : The socket we are working on
     * @param clientHandshake
     */

    @Override
    public void onOpen(WebSocket webSocket, ClientHandshake clientHandshake) {
        System.out.println("open");
        webSocket.send("Welcome SIn11");
        v.add(webSocket);


    }

    /**
     * <p>Method inherited from WebSocketServer</p>
     * <p>Used to close te connection</p>
     * @param webSocket : the socket we are working on
     * @param i
     * @param s
     * @param b
     */
    @Override
    public void onClose(WebSocket webSocket, int i, String s, boolean b) {

        v.remove(webSocket);
    }

    /**
     * <p>Method inherited from WebSocketServer</p>
     * <p>The method takes a raw String from the web page.</p>
     * <p>It splits the message in half around the "=" sign.</p>
     * <p>It determines if the message is a Boolean or a Float Datapoint</p>
     * <p>It sets the value of the Datapoint accordingly of the Label and Value received.</p>
     * @param webSocket : the socket we are working on
     * @param s : The message we want to read
     *
     */
    @Override
    public void onMessage(WebSocket webSocket, String s) {
        System.out.println(s);
        String[] message = s.split("=");
        String label=message[0];
        String value = message[1];
        if (value.equals("true") || value.equals("false")) {
            BinaryDataPoint bdp;
            bdp = (BinaryDataPoint)BinaryDataPoint.getDataPointFromLabel(label);
            bdp.setValue(Boolean.parseBoolean(value));
            System.out.println("bdp with :  "+bdp.getLabel() + " "+ bdp.getValue());
        }
        else{
            FloatDataPoint fdp;
            fdp = (FloatDataPoint)FloatDataPoint.getDataPointFromLabel(label);
            fdp.setValue(Float.parseFloat(value));

            System.out.println("fdp with :  "+fdp.getLabel() + " "+ fdp.getValue());
        }

    }

    /**
     * <p>Method inherited from WebSocketServer</p>
     * <p>Print the result of the error</p>
     * @param webSocket : the socket we are working on
     * @param e : the error to catch
     */
    @Override
    public void onError(WebSocket webSocket, Exception e) {
            e.printStackTrace();
    }
    /**
     * <p>Method inherited from WebSocketServer</p>
     * <p>Print in the console to verify the TCP is well established</p>

     */
    @Override
    public void onStart() {
        System.out.println("started");
    }

    /**
     * <p>A main method used to test this class</p>
     * @param args
     */
    public static void main(String[] args) {
        Launcher l = new Launcher();
        l.launch();


        FloatDataPoint fp1=new FloatDataPoint("GRID_U_FLOAT",false,222);
        while(true){
            fp1.setValue(fp1.getValue()+1f);

            Utility.waitSomeTime(3000);
        }
        //FloatDataPoint fp1=new FloatDataPoint("GRID_U_FLOAT",false,222.22f);


        //fp1.setValue(234.45f);

    }
}

